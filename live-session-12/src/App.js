import axios from 'axios';
import { useContext, useEffect } from 'react';
import './App.css';
import ArticleContainer from './components/articleContainer/ArticleContainer';
import ArticleTable from './components/articleTable/ArticleTable';
import EditForm from './components/editForm/EditForm';
import Form from './components/form/Form';
import { GlobalContext } from './context/GlobalContext';

function App() {
	const { setArticles, edit, loading, setLoading, dispatch } = useContext(GlobalContext);

	const fetchArticles = async () => {
		setLoading(true);
		try {
			const response = await axios.get('http://localhost:8000/api/articles');
			setArticles(response.data.data);
		} catch (error) {
			console.log(error);
		} finally {
			setLoading(false);
		}
	};

	useEffect(() => {
		fetchArticles();
	}, []);

	return loading === true ? (
		<h1>Loading</h1>
	) : (
		<div className="App">
			<div style={{ display: 'flex', justifyContent: 'center' }}>
				<button onClick={() => dispatch({ type: 'DECREMENT' })}>Kurang</button>
				<button onClick={() => dispatch({ type: 'INCREMENT' })}>Tambah</button>
			</div>
			{Object.keys(edit).length === 0 ? (
				<Form fetchArticles={fetchArticles} />
			) : (
				<EditForm fetchArticles={fetchArticles} />
			)}
			<ArticleTable fetchArticles={fetchArticles} />
			{loading === true ? (
				<div style={{ textAlign: 'center', fontSize: '20px', fontWeight: 'bold' }}>Loading....</div>
			) : (
				<ArticleContainer />
			)}
		</div>
	);
}

export default App;
