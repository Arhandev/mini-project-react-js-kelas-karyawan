import React, { useContext } from 'react';
import { GlobalContext } from '../../context/GlobalContext';
import Article from '../article/Article';
import './articleContainer.css';

function ArticleContainer() {
	const { articles, state } = useContext(GlobalContext);
	return (
		<div className="article-container">
			<h1 style={{ textAlign: 'center' }}>{state.count}</h1>
			<h1>Preview Artikel</h1>
			{articles.map(article => (
				<Article article={article} />
			))}
		</div>
	);
}

export default ArticleContainer;
