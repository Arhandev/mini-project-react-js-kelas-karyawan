import React from 'react';
import { useParams } from 'react-router-dom';

function ProfileDetail() {
	const { user } = useParams();
	return (
		<div>
			<h1>Ini adalah halaman {user}</h1>
		</div>
	);
}

export default ProfileDetail;
