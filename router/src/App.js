import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import './App.css';
import Home from './components/Home';
import Login from './components/Login';
import Profile from './components/Profile';
import ProfileDetail from './components/ProfileDetail';

const router = createBrowserRouter([
	{
		path: '/profile',
		element: <Profile />,
	},
	{
		path: '/profile/:user',
		element: <ProfileDetail />,
	},
	{
		path: '/',
		element: <Home />,
	},
	{
		path: '/login',
		element: <Login />,
	},
]);

function App() {
	return (
		<div className="App">
			<RouterProvider router={router} />
		</div>
	);
}

export default App;
