import { useContext } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import './App.css';
import ArticleContainer from './components/articleContainer/ArticleContainer';
import ArticleTable from './components/articleTable/ArticleTable';
import EditForm from './components/editForm/EditForm';
import Form from './components/form/Form';
import { GlobalContext } from './context/GlobalContext';

const router = createBrowserRouter([
	{
		path: '/',
		element: <ArticleContainer />,
	},
	{
		path: '/setting',
		element: <ArticleTable />,
	},
	{
		path: '/create',
		element: <Form />,
	},
	{
		path: '/edit/:id',
		element: <EditForm />,
	},
]);

function App() {
	const { setArticles, edit, loading, setLoading } = useContext(GlobalContext);

	return (
		<div className="App">
			<RouterProvider router={router} />
		</div>
	);
}

export default App;
