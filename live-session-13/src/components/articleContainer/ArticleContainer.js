import axios from 'axios';
import React, { useContext, useEffect } from 'react';
import { GlobalContext } from '../../context/GlobalContext';
import Article from '../article/Article';
import Navbar from '../Navbar';
import './articleContainer.css';

function ArticleContainer() {
	const { articles, loading, setLoading, setArticles } = useContext(GlobalContext);
	const fetchArticles = async () => {
		setLoading(true);
		try {
			const response = await axios.get('http://localhost:8000/api/articles');
			setArticles(response.data.data);
		} catch (error) {
			console.log(error);
		} finally {
			setLoading(false);
		}
	};

	useEffect(() => {
		fetchArticles();
	}, []);

	return loading === true ? (
		<div style={{ textAlign: 'center', fontSize: '20px', fontWeight: 'bold' }}>Loading....</div>
	) : (
		<div>
			<Navbar />
			<div className="article-container">
				<h1>Preview Artikel</h1>
				{articles.map(article => (
					<Article article={article} />
				))}
			</div>
		</div>
	);
}

export default ArticleContainer;
