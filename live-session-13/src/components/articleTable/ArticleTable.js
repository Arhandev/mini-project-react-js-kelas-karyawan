import axios from 'axios';
import React, { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { GlobalContext } from '../../context/GlobalContext';
import Navbar from '../Navbar';
import './ArticleTable.css';

function ArticleTable() {
	const { articles, setEdit, setLoading, setArticles } = useContext(GlobalContext);
	const onDelete = async articleId => {
		try {
			const response = await axios.delete(`http://localhost:8000/api/articles/${articleId}`);
			fetchArticles();
		} catch (error) {
			alert(error.response.data.info);
		}
	};

	const fetchArticles = async () => {
		setLoading(true);
		try {
			const response = await axios.get('http://localhost:8000/api/articles');
			setArticles(response.data.data);
		} catch (error) {
			console.log(error);
		} finally {
			setLoading(false);
		}
	};

	useEffect(() => {
		fetchArticles();
	}, []);

	return (
		<div>
			<Navbar />
			<div>
				<div style={{ maxWidth: '800px', margin: 'auto', display: 'flex', justifyContent: 'flex-end' }}>
					<Link to={'/create'}>
						<button style={{ padding: '20px', backgroundColor: 'blue', color: 'white', fontSize: '15px' }}>
							Buat Artikel
						</button>
					</Link>
				</div>
				<table>
					<thead>
						<tr>
							<th>Gambar</th>
							<th>Judul</th>
							<th>Konten</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{articles.map(article => {
							return (
								<tr>
									<td>
										<img src={article.image_url} width="200" alt="" />
									</td>
									<td>{article.judul}</td>
									<td>{article.konten}</td>
									<td>
										<div className="action">
											<Link to={`/edit/${article.id}`}>
												<button className="btn btn-edit">Edit</button>
											</Link>
											<button
												onClick={() => {
													onDelete(article.id);
												}}
												className="btn btn-delete"
											>
												Delete
											</button>
										</div>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			</div>
		</div>
	);
}

export default ArticleTable;
