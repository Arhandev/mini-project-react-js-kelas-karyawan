import React from 'react';
import './article.css';

function Article(props) {
	return (
		<div className="article">
			<div className="image-container">
				<img src={props.article.image_url} alt="" />
			</div>
			<div className="content-container">
				<h1>{props.article.judul}</h1>
				<p>{props.article.konten}</p>
			</div>
		</div>
	);
}

export default Article;
