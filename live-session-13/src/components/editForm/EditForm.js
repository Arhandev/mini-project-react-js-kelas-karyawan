import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { GlobalContext } from '../../context/GlobalContext';
import Navbar from '../Navbar';
import './form.css';

function EditForm({ fetchArticles }) {
	const { loading, setLoading } = useContext(GlobalContext);
	const params = useParams();
	const navigate = useNavigate();
	const [input, setInput] = useState({
		judul: '',
		konten: '',
		image_url: '',
	});

	const fetchArticle = async () => {
		setLoading(true);
		try {
			const response = await axios.get(`http://localhost:8000/api/articles/${params.id}`);
			setInput({
				judul: response.data.data.judul,
				konten: response.data.data.konten,
				image_url: response.data.data.image_url,
			});
		} catch (error) {
			console.log(error);
		} finally {
			setLoading(false);
		}
	};

	const handleChange = event => {
		if (event.target.name === 'judul') {
			setInput({ ...input, judul: event.target.value });
		} else if (event.target.name === 'konten') {
			setInput({ ...input, konten: event.target.value });
		} else if (event.target.name === 'image_url') {
			setInput({ ...input, image_url: event.target.value });
		}
	};
	const onSubmit = async () => {
		setLoading(true);
		try {
			const response = await axios.put(`http://localhost:8000/api/articles/${params.id}`, {
				judul: input.judul,
				konten: input.konten,
				image_url: input.image_url,
			});
			setInput({ judul: '', konten: '', image_url: '' });
			navigate('/setting');
		} catch (error) {
			alert(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};

	useEffect(() => {
		fetchArticle();
	}, []);

	return (
		<div>
			<Navbar />
			<div className="form">
				<h2 style={{ textAlign: 'center' }}>Edit Form</h2>
				<form className="form-input">
					<div className="form-input-group">
						<label>Judul:</label>
						<input type="text" placeholder="Masukkan judul" onChange={handleChange} name="judul" value={input.judul} />
					</div>
					<div className="form-input-group">
						<label>Konten:</label>
						<input
							type="text"
							placeholder="Masukkan konten"
							onChange={handleChange}
							name="konten"
							value={input.konten}
						/>
					</div>
					<div className="form-input-group">
						<label>Image Url:</label>
						<input
							type="text"
							placeholder="Masukkan link gambar"
							onChange={handleChange}
							name="image_url"
							value={input.image_url}
						/>
					</div>
					<div className="button-container">
						<button type="button" onClick={onSubmit} disabled={loading}>
							{loading === true ? 'Loading' : 'Simpan'}
						</button>
					</div>
				</form>
			</div>
		</div>
	);
}

export default EditForm;
