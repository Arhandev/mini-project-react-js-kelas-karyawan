import axios from 'axios';
import React from 'react';
import './ArticleTable.css';

function ArticleTable({ articles, edit, setEdit, fetchArticles }) {
	const onDelete = async (articleId) => {
		try {
			const response = await axios.delete(`http://localhost:8000/api/articles/${articleId}`);
			fetchArticles();
		} catch (error) {
			alert(error.response.data.info);
		}
	};

	return (
		<div>
			<table>
				<thead>
					<tr>
						<th>Gambar</th>
						<th>Judul</th>
						<th>Konten</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{articles.map(article => {
						return (
							<tr>
								<td>
									<img src={article.image_url} width="200" alt="" />
								</td>
								<td>{article.judul}</td>
								<td>{article.konten}</td>
								<td>
									<div className="action">
										<button
											onClick={() => {
												setEdit(article);
											}}
											className="btn btn-edit"
										>
											Edit
										</button>
										<button
											onClick={() => {
												onDelete(article.id);
											}}
											className="btn btn-delete"
										>
											Delete
										</button>
									</div>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
}

export default ArticleTable;
