import axios from 'axios';
import { useEffect, useState } from 'react';
import './App.css';
import ArticleContainer from './components/articleContainer/ArticleContainer';
import ArticleTable from './components/articleTable/ArticleTable';
import EditForm from './components/editForm/EditForm';
import Form from './components/form/Form';

function App() {
	const [articles, setArticles] = useState([]);
	const [loading, setLoading] = useState(false);
	const [edit, setEdit] = useState({});
	const fetchArticles = async () => {
		setLoading(true);
		try {
			const response = await axios.get('http://localhost:8000/api/articles');
			setArticles(response.data.data);
		} catch (error) {
			console.log(error);
		} finally {
			setLoading(false);
		}
	};

	useEffect(() => {
		fetchArticles();
	}, []);

	return (
		<div className="App">
			{Object.keys(edit).length === 0 ? (
				<Form articles={articles} setArticles={setArticles} fetchArticles={fetchArticles} />
			) : (
				<EditForm edit={edit} setEdit={setEdit} fetchArticles={fetchArticles} />
			)}
			<ArticleTable
				articles={articles}
				setArticles={setArticles}
				fetchArticles={fetchArticles}
				edit={edit}
				setEdit={setEdit}
			/>
			{loading === true ? (
				<div style={{ textAlign: 'center', fontSize: '20px', fontWeight: 'bold' }}>Loading....</div>
			) : (
				<ArticleContainer articles={articles} />
			)}
		</div>
	);
}

export default App;
