import { useReducer, useState } from 'react';
import './App.css';
import { initialState, reducer } from './reducer/counterReducer';


function App() {
	const [state, dispatch] = useReducer(reducer, initialState);
	return (
		<div className="App">
			<h1>{state.count}</h1>
			<button
				onClick={() => {
					dispatch({ type: 'DECREMENT' });
				}}
			>
				Kurang
			</button>
			<button
				onClick={() => {
					dispatch({ type: 'INCREMENT' });
				}}
			>
				Tambah
			</button>
		</div>
	);
}

export default App;
