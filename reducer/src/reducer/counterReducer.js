export const initialState = {
	count: 0,
};

export const reducer = (state, action) => {
	const { type } = action;
	if (type === 'INCREMENT') {
		return { count: state.count + 1 };
	} else if (type === 'DECREMENT') {
		return { count: state.count - 1 };
	}
};
