import { useState } from 'react';
import './App.css';
import ArticleContainer from './components/articleContainer/ArticleContainer';
import Form from './components/form/Form';

function App() {
	const initialArticles = [
		{
			judul: 'Ini artikel 1',
			konten: 'Halo dunia ini dari artikel 1',
			image_url: 'https://jualanku.link/admin/get_file/cfd34290d36c11ecb199fbaa30989ce3_1597.png',
		},
		{
			judul: 'Ini artikel 2',
			konten: 'Halo dunia ini dari artikel 2',
			image_url: 'https://jualanku.link/admin/get_file/39c43670d37211ec910a8706799609fa_1598.png',
		},
	];
	const [articles, setArticles] = useState(initialArticles);
	return (
		<div className="App">
			<Form articles={articles} setArticles={setArticles} />
			<ArticleContainer articles={articles} />
		</div>
	);
}

export default App;
