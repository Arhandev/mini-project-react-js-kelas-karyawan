import React, { useState } from 'react';
import './form.css';

function Form(props) {
	const [input, setInput] = useState({
		judul: '',
		konten: '',
		image_url: '',
	});

	const handleChange = event => {
		console.log(event.target.name);
		if (event.target.name === 'judul') {
			setInput({ ...input, judul: event.target.value });
		} else if (event.target.name === 'konten') {
			setInput({ ...input, konten: event.target.value });
		} else if (event.target.name === 'image_url') {
			setInput({ ...input, image_url: event.target.value });
		}
	};

	const onSubmit = () => {
		props.setArticles([...props.articles, input]);
		setInput({ judul: '', konten: '', image_url: '' });
	};
	return (
		<div className="form">
			<h2 style={{ textAlign: 'center' }}>Form</h2>
			<form className="form-input">
				<div className="form-input-group">
					<label>Judul:</label>
					<input type="text" placeholder="Masukkan judul" onChange={handleChange} name="judul" value={input.judul} />
				</div>
				<div className="form-input-group">
					<label>Konten:</label>
					<input type="text" placeholder="Masukkan konten" onChange={handleChange} name="konten" value={input.konten} />
				</div>
				<div className="form-input-group">
					<label>Image Url:</label>
					<input
						type="text"
						placeholder="Masukkan link gambar"
						onChange={handleChange}
						name="image_url"
						value={input.image_url}
					/>
				</div>
				<div className="button-container">
					<button type="button" onClick={onSubmit}>
						Simpan
					</button>
				</div>
			</form>
		</div>
	);
}

export default Form;
