import { useContext } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import './App.css';
import ArticleContainer from './components/articleContainer/ArticleContainer';
import ArticleTable from './components/articleTable/ArticleTable';
import EditForm from './components/editForm/EditForm';
import Form from './components/form/Form';
import Login from './components/login/Login';
import Profile from './components/profile/Profile';
import Register from './components/register/Register';
import { GlobalContext } from './context/GlobalContext';
import ProtectedRoute from './wrapper/ProtectedRoute';

const router = createBrowserRouter([
	{
		path: '/',
		element: <ArticleContainer />,
	},
	{
		path: '/login',
		element: <Login />,
	},
	{
		path: '/register',
		element: <Register />,
	},
	{
		element: <ProtectedRoute />,
		children: [
			{
				path: '/setting',
				element: <ArticleTable />,
			},
			{
				path: '/profile',
				element: <Profile />,
			},
			{
				path: '/create',
				element: <Form />,
			},
			{
				path: '/edit/:id',
				element: <EditForm />,
			},
		],
	},
]);

function App() {
	const { setArticles, edit, loading, setLoading } = useContext(GlobalContext);

	return (
		<div className="App">
			<RouterProvider router={router} />
		</div>
	);
}

export default App;
