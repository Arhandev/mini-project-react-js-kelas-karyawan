import axios from 'axios';
import { Formik } from 'formik';
import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { GlobalContext } from '../../context/GlobalContext';
import Navbar from '../Navbar';

const validationSchema = Yup.object({
	email: Yup.string().required('Email Wajib diisi').email('Email tidak valid'),
	name: Yup.string().required('Nama wajib diisi'),
	username: Yup.string().required('Username wajib diisi'),
	password: Yup.string().required('Password wajib diisi'),
	password_confirmation: Yup.string().required('Konfirmasi password wajib diisi'),
});

function Register() {
	const initialState = {
		email: '',
		name: '',
		username: '',
		password: '',
		password_confirmation: '',
	};
	const navigate = useNavigate();
	const { loading, setLoading } = useContext(GlobalContext);

	const onSubmit = async values => {
		setLoading(true);
		try {
			const response = await axios.post('https://arhandev.xyz/public/api/register', {
				email: values.email,
				name: values.name,
				username: values.username,
				password: values.password,
				password_confirmation: values.password_confirmation,
			});
			alert('Registrasi akun berhasil');
			navigate('/login');
		} catch (error) {
			alert(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};

	return (
		<div>
			<Navbar />
			<div className="form">
				<h2 style={{ textAlign: 'center' }}>Register Form</h2>

				<Formik onSubmit={onSubmit} initialValues={initialState} validationSchema={validationSchema}>
					{params => {
						return (
							<form onSubmit={params.handleSubmit} className="mx-8 flex flex-col gap-4 my-8 items-center">
								<div className="w-full grid grid-cols-3 text-lg">
									<label>Email:</label>
									<input
										type="text"
										placeholder="Masukkan email"
										onChange={params.handleChange}
										value={params.values.email}
										onBlur={params.handleBlur}
										name="email"
										className="col-span-2 border-black border-2 rounded-md pl-2"
									/>
									<div></div>
									<div>{params.touched.email && params.errors.email}</div>
								</div>
								<div className="w-full grid grid-cols-3 text-lg">
									<label>Name:</label>
									<input
										type="text"
										placeholder="Masukkan nama"
										onChange={params.handleChange}
										value={params.values.name}
										onBlur={params.handleBlur}
										name="name"
										className="col-span-2 border-black border-2 rounded-md pl-2"
									/>
									<div></div>
									<div>{params.touched.name && params.errors.name}</div>
								</div>
								<div className="w-full grid grid-cols-3 text-lg">
									<label>Username:</label>
									<input
										type="text"
										placeholder="Masukkan username"
										onChange={params.handleChange}
										value={params.values.username}
										onBlur={params.handleBlur}
										name="username"
										className="col-span-2 border-black border-2 rounded-md pl-2"
									/>
									<div></div>
									<div>{params.touched.username && params.errors.username}</div>
								</div>
								<div className="w-full grid grid-cols-3 text-lg">
									<label>Password:</label>
									<input
										type="password"
										placeholder="Masukkan password"
										name="password"
										onChange={params.handleChange}
										onBlur={params.handleBlur}
										value={params.values.password}
										className="col-span-2 border-black border-2 rounded-md pl-2"
									/>
									<div></div>
									<div>{params.touched.password && params.errors.password}</div>
								</div>
								<div className="w-full grid grid-cols-3 text-lg">
									<label>Konfirmasi Password:</label>
									<input
										type="password"
										placeholder="Masukkan konfirmasi password"
										name="password_confirmation"
										onChange={params.handleChange}
										onBlur={params.handleBlur}
										value={params.values.password_confirmation}
										className="col-span-2 border-black border-2 rounded-md pl-2"
									/>
									<div></div>
									<div>{params.touched.password_confirmation && params.errors.password_confirmation}</div>
								</div>
								<div className="button-container">
									<button type="submit" disabled={loading}>
										{loading === true ? 'Loading' : 'Register'}
									</button>
								</div>
							</form>
						);
					}}
				</Formik>
			</div>
		</div>
	);
}

export default Register;
