import axios from 'axios';
import React from 'react';
import { Link, useNavigate } from 'react-router-dom';

function Navbar() {
	const navigate = useNavigate();

	const onLogout = async () => {
		try {
			const response = await axios.post(
				'https://arhandev.xyz/public/api/logout',
				{},
				{
					headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
				}
			);
			localStorage.removeItem('token');
			localStorage.removeItem('username');
			navigate('/');
		} catch (error) {
			alert(error.response.data.info);
		}
	};
	return (
		<div className="bg-green-900">
			<header className="w-10/12 mx-auto py-6 flex justify-between items-center">
				<nav className="flex items-center gap-8 text-white text-2xl">
					<div>
						<Link to={'/'}>Home</Link>
					</div>
					<div>
						<Link to={'/setting'}>Setting</Link>
					</div>
					<div>
						<Link to={'/profile'}>Profile</Link>
					</div>
				</nav>
				<nav>
					{localStorage.getItem('token') === null ? (
						<div className="flex items-center gap-4">
							<Link to={'/login'}>
								<button className="bg-green-200 border-2 border-green-200 px-7 py-2 rounded-xl text-xl text-green-900 font-bold">
									Login
								</button>
							</Link>
							<Link to={'/register'}>
								<button className="bg-green-200 border-2 border-green-200 px-7 py-2 rounded-xl text-xl text-green-900 font-bold">
									Register
								</button>
							</Link>
						</div>
					) : (
						<div className="flex gap-4 items-center">
							<p className="text-2xl text-white">Hi, {localStorage.getItem('username')}</p>
							<button
								onClick={onLogout}
								className="bg-red-200 border-2 border-red-200 px-7 py-2 rounded-xl text-xl text-red-900 font-bold"
							>
								Logout
							</button>
						</div>
					)}
				</nav>
			</header>
		</div>
	);
}

export default Navbar;
