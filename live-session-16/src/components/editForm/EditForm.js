import axios from 'axios';
import { Formik } from 'formik';
import React, { useContext, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import * as Yup from 'yup';
import { GlobalContext } from '../../context/GlobalContext';
import Navbar from '../Navbar';
import './form.css';

const initialState = {
	judul: '',
	konten: '',
	image_url: '',
	highlight: 0,
};

const validationSchema = Yup.object({
	judul: Yup.string().required(),
	konten: Yup.string().required(),
	image_url: Yup.string().required().url(),
	highlight: Yup.bool(),
});

function EditForm() {
	const { loading, setLoading } = useContext(GlobalContext);
	const params = useParams();
	const navigate = useNavigate();
	const [input, setInput] = useState(initialState);

	const fetchArticle = async () => {
		setLoading(true);
		try {
			const response = await axios.get(`https://arhandev.xyz/public/api/auth/articles/${params.id}`, {
				headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
			});
			setInput(
				{
					judul: response.data.data.judul,
					konten: response.data.data.konten,
					image_url: response.data.data.image_url,
					highlight: response.data.data.highlight,
				},
				{ headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } }
			);
		} catch (error) {
			console.log(error);
		} finally {
			setLoading(false);
		}
	};

	const onSubmit = async values => {
		console.log(values);
		setLoading(true);
		try {
			const response = await axios.put(
				`https://arhandev.xyz/public/api/auth/articles/${params.id}`,
				{
					judul: values.judul,
					konten: values.konten,
					image_url: values.image_url,
					highlight: values.highlight,
				},
				{ headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } }
			);
			navigate('/setting');
		} catch (error) {
			alert(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};

	useEffect(() => {
		fetchArticle();
	}, []);

	return (
		<div>
			<Navbar />
			<div className="form">
				<h2 style={{ textAlign: 'center' }}>Edit Form</h2>
				<Formik onSubmit={onSubmit} initialValues={input} enableReinitialize validationSchema={validationSchema}>
					{params => (
						<form onSubmit={params.handleSubmit} className="mx-8 flex flex-col gap-4 my-8 items-center">
							<div className="w-full grid grid-cols-3 text-lg">
								<label>Judul:</label>
								<input
									type="text"
									placeholder="Masukkan judul"
									onChange={params.handleChange}
									value={params.values.judul}
									onBlur={params.handleBlur}
									name="judul"
									className="col-span-2 border-black border-2 rounded-md pl-2"
								/>
								<div></div>
								<div>{params.touched.judul && params.errors.judul}</div>
							</div>
							<div className="w-full grid grid-cols-3 text-lg">
								<label>Konten:</label>
								<input
									type="text"
									placeholder="Masukkan konten"
									onChange={params.handleChange}
									onBlur={params.handleBlur}
									value={params.values.konten}
									name="konten"
									className="col-span-2 border-black border-2 rounded-md pl-2"
								/>
								<div></div>
								<div>{params.touched.konten && params.errors.konten}</div>
							</div>

							<div className="w-full grid grid-cols-3 text-lg">
								<label>Image Url:</label>
								<input
									type="text"
									placeholder="Masukkan link gambar"
									onChange={params.handleChange}
									onBlur={params.handleBlur}
									value={params.values.image_url}
									name="image_url"
									className="col-span-2 border-black border-2 rounded-md pl-2"
								/>
								<div></div>
								<div>{params.touched.image_url && params.errors.image_url}</div>
							</div>
							<div className="w-full grid grid-cols-3 text-lg">
								<label>Highlight</label>
								<input
									style={{ justifySelf: 'flex-start' }}
									type="checkbox"
									name="highlight"
									onChange={e => {
										if (e.target.checked) {
											params.setFieldValue('highlight', 1);
										} else {
											params.setFieldValue('highlight', 0);
										}
									}}
									onBlur={() => {
										params.setFieldTouched('highlight');
									}}
									checked={params.values.highlight}
								/>
								<div></div>
								<div>{params.touched.highlight && params.errors.highlight}</div>
							</div>
							<div className="button-container">
								<button type="submit" disabled={loading}>
									{loading === true ? 'Loading' : 'Simpan'}
								</button>
							</div>
						</form>
					)}
				</Formik>
			</div>
		</div>
	);
}

export default EditForm;
