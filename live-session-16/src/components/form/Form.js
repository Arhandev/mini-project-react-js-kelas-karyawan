import axios from 'axios';
import { Formik } from 'formik';
import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { GlobalContext } from '../../context/GlobalContext';
import Navbar from '../Navbar';
import './form.css';

const validationSchema = Yup.object({
	judul: Yup.string().required(),
	konten: Yup.string().required(),
	image_url: Yup.string().required().url(),
	highlight: Yup.bool(),
	// tipe: Yup.string().oneOf(['semua', 'berita'], 'Dilarang memilih tipe resep'),
});

function Form() {
	const initialState = {
		judul: '',
		konten: '',
		image_url: '',
		highlight: 0,
		// tipe: 'semua',
	};
	const navigate = useNavigate();
	const { loading, setLoading } = useContext(GlobalContext);

	const onSubmit = async values => {
		setLoading(true);
		try {
			const response = await axios.post(
				'https://arhandev.xyz/public/api/auth/articles',
				{
					judul: values.judul,
					konten: values.konten,
					image_url: values.image_url,
					highlight: values.highlight,
				},
				{ headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } }
			);
			navigate('/setting');
		} catch (error) {
			alert(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};

	return (
		<div>
			<Navbar />
			<div className="form">
				<h2 className="text-center text-3xl font-bold">Create Form</h2>

				<Formik onSubmit={onSubmit} initialValues={initialState} validationSchema={validationSchema}>
					{params => {
						return (
							<form onSubmit={params.handleSubmit} className="mx-8 flex flex-col gap-4 my-8 items-center">
								<div className="w-full grid grid-cols-3 text-lg">
									<label>Judul:</label>
									<input
										type="text"
										placeholder="Masukkan judul"
										onChange={params.handleChange}
										value={params.values.judul}
										onBlur={params.handleBlur}
										name="judul"
										className="col-span-2 border-black border-2 rounded-md pl-2"
									/>
									<div></div>
									<div>{params.touched.judul && params.errors.judul}</div>
								</div>
								<div className="w-full grid grid-cols-3 text-lg">
									<label>Konten:</label>
									<input
										type="text"
										placeholder="Masukkan konten"
										name="konten"
										onChange={params.handleChange}
										onBlur={params.handleBlur}
										value={params.values.konten}
										className="col-span-2 border-black border-2 rounded-md pl-2"
									/>
									<div></div>
									<div>{params.touched.konten && params.errors.konten}</div>
								</div>
								<div className="w-full grid grid-cols-3 text-lg">
									<label>Image Url:</label>
									<input
										type="text"
										placeholder="Masukkan link gambar"
										name="image_url"
										onChange={params.handleChange}
										onBlur={params.handleBlur}
										value={params.values.image_url}
										className="col-span-2 border-black border-2 rounded-md pl-2"
									/>
									<div></div>
									<div>{params.touched.image_url && params.errors.image_url}</div>
								</div>
								<div className="w-full grid grid-cols-3 text-lg">
									<label>Highlight</label>
									<input
										style={{ justifySelf: 'flex-start' }}
										type="checkbox"
										name="highlight"
										onChange={e => {
											if (e.target.checked) {
												params.setFieldValue('highlight', 1);
											} else {
												params.setFieldValue('highlight', 0);
											}
										}}
										onBlur={() => {
											params.setFieldTouched('highlight');
										}}
									/>
									<div></div>
									<div>{params.touched.highlight && params.errors.highlight}</div>
								</div>
								{/* <div className="w-full grid grid-cols-3 text-lg">
									<label>Tipe</label>
									<div>
										<select
											name="tipe"
											id=""
											onChange={e => {
												params.setFieldValue('tipe', e.target.value);
											}}
											onBlur={() => {
												params.setFieldTouched('tipe');
											}}
										>
											<option value="semua">Semua</option>
											<option value="berita">berita</option>
											<option value="resep">resep</option>
										</select>
									</div>
									<div></div>
									<div></div>
									<div>{params.touched.tipe && params.errors.tipe}</div>
								</div> */}
								<div className="button-container">
									<button type="submit" disabled={loading}>
										{loading === true ? 'Loading' : 'Simpan'}
									</button>
								</div>
							</form>
						);
					}}
				</Formik>
			</div>
		</div>
	);
}

export default Form;
