import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { GlobalContext } from '../../context/GlobalContext';
import Navbar from '../Navbar';
import './ArticleTable.css';

function ArticleTable() {
	const { articles, setEdit, setLoading, setArticles } = useContext(GlobalContext);
	const [search, setSearch] = useState('');
	const [filterHighlight, setFilterHighlight] = useState('');
	const [displayData, setDisplayData] = useState([]);

	const onDelete = async articleId => {
		try {
			const response = await axios.delete(`https://arhandev.xyz/public/api/articles/${articleId}`);
			fetchArticles();
		} catch (error) {
			alert(error.response.data.info);
		}
	};

	const fetchArticles = async () => {
		setLoading(true);
		try {
			const response = await axios.get('https://arhandev.xyz/public/api/auth/articles', {
				headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
			});
			setArticles(response.data.data);
			setDisplayData(response.data.data);
		} catch (error) {
			console.log(error);
		} finally {
			setLoading(false);
		}
	};

	const handleChange = event => {
		setSearch(event.target.value);
	};

	const handleChangeSelect = event => {
		setFilterHighlight(event.target.value);
	};

	const onFilter = () => {
		let arrFilter = [...articles];
		if (filterHighlight !== '') {
			arrFilter = arrFilter.filter(article => article.highlight == filterHighlight);
		}

		if (search !== '') {
			arrFilter = arrFilter.filter(article => article.judul.toLowerCase().includes(search.toLowerCase()));
		}
		setDisplayData(arrFilter);
	};

	const onReset = () => {
		setDisplayData(articles);
		setSearch('');
		setFilterHighlight('');
	};

	useEffect(() => {
		fetchArticles();
	}, []);

	return (
		<div>
			<Navbar />
			<div style={{ margin: '20px 0px' }}>
				<div className="max-w-5xl mx-auto flex justify-end">
					<Link to={'/create'}>
						<button style={{ padding: '20px', backgroundColor: 'blue', color: 'white', fontSize: '15px' }}>
							Buat Artikel
						</button>
					</Link>
				</div>
				<div className="mx-auto flex justify-end items-center gap-4 max-w-5xl mt-6 mb-4">
					<select name="highlight" onChange={handleChangeSelect} value={filterHighlight} id="">
						<option disabled selected>
							Pilih Highlight
						</option>
						<option value="">Semua</option>
						<option value="0">Mati</option>
						<option value="1">Aktif</option>
					</select>
					<input
						onChange={handleChange}
						value={search}
						name="search"
						type="text"
						className="border-2 border-gray-600"
						placeholder="search..."
					/>
					<button onClick={onFilter} className="border px-4 py-2 bg-blue-200 rounded-md">
						Filter
					</button>
					<button onClick={onReset} className="border px-4 py-2 bg-red-200 rounded-md">
						Reset
					</button>
				</div>
				<table className="max-w-5xl mx-auto border-collapse">
					<thead>
						<tr>
							<th>Gambar</th>
							<th>Judul</th>
							<th>Konten</th>
							<th>Highlight</th>
							<th>Dibuat oleh</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{displayData.map(article => {
							return (
								<tr>
									<td>
										<img src={article.image_url} width="200" alt="" />
									</td>
									<td>{article.judul}</td>
									<td>{article.konten}</td>
									<td>{article.highlight == 1 ? 'aktif' : 'mati'}</td>
									<td>{article.user.username}</td>
									<td>
										<div className="flex gap-3">
											<Link to={`/edit/${article.id}`}>
												<button className="px-5 py-3 text-white font-bold rounded-lg bg-yellow-600">Edit</button>
											</Link>
											<button
												onClick={() => {
													onDelete(article.id);
												}}
												className="px-5 py-3 text-white font-bold rounded-lg bg-red-600"
											>
												Delete
											</button>
										</div>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			</div>
		</div>
	);
}

export default ArticleTable;
