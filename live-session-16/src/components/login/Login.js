import axios from 'axios';
import { Formik } from 'formik';
import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { GlobalContext } from '../../context/GlobalContext';
import Navbar from '../Navbar';

const validationSchema = Yup.object({
	email: Yup.string().required(),
	password: Yup.string().required(),
});

function Login() {
	const initialState = {
		email: '',
		password: '',
	};
	const navigate = useNavigate();
	const { loading, setLoading } = useContext(GlobalContext);

	const onSubmit = async values => {
		setLoading(true);
		try {
			const response = await axios.post('https://arhandev.xyz/public/api/login', {
				email: values.email,
				password: values.password,
			});
			localStorage.setItem('token', response.data.data.token);
			localStorage.setItem('username', response.data.data.user.username);
			navigate('/');
		} catch (error) {
			alert(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};

	return (
		<div>
			<Navbar />
			<div className="form">
				<h2 style={{ textAlign: 'center' }}>Login Form</h2>

				<Formik onSubmit={onSubmit} initialValues={initialState} validationSchema={validationSchema}>
					{params => {
						return (
							<form onSubmit={params.handleSubmit} className="mx-8 flex flex-col gap-4 my-8 items-center">
								<div className="w-full grid grid-cols-3 text-lg">
									<label>Email:</label>
									<input
										type="text"
										placeholder="Masukkan email"
										onChange={params.handleChange}
										value={params.values.email}
										onBlur={params.handleBlur}
										name="email"
										className="col-span-2 border-black border-2 rounded-md"
									/>
									<div></div>
									<div>{params.touched.email && params.errors.email}</div>
								</div>
								<div className="w-full grid grid-cols-3 text-lg">
									<label>Password:</label>
									<input
										type="password"
										placeholder="Masukkan password"
										name="password"
										onChange={params.handleChange}
										onBlur={params.handleBlur}
										value={params.values.password}
										className="col-span-2 border-black border-2 rounded-md"
									/>
									<div></div>
									<div>{params.touched.password && params.errors.password}</div>
								</div>
								<div className="button-container">
									<button type="submit" disabled={loading}>
										{loading === true ? 'Loading' : 'login'}
									</button>
								</div>
							</form>
						);
					}}
				</Formik>
			</div>
		</div>
	);
}

export default Login;
