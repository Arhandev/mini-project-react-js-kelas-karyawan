import { createContext, useReducer, useState } from 'react';

export const GlobalContext = createContext();

const initialState = {
	count: 0,
};

const countReducer = (state, action) => {
	const { type } = action;
	if (type === 'INCREMENT') {
		return { count: state.count + 1 };
	} else if (type === 'DECREMENT') {
		return { count: state.count - 1 };
	}
};

export const GlobalProvider = props => {
	const [articles, setArticles] = useState([]);
	const [edit, setEdit] = useState({});
	const [loading, setLoading] = useState(false);
	const [state, dispatch] = useReducer(countReducer, initialState);

	return (
		<GlobalContext.Provider
			value={{
				articles: articles,
				setArticles: setArticles,
				edit: edit,
				setEdit: setEdit,
				loading: loading,
				setLoading: setLoading,
				state: state,
				dispatch: dispatch,
			}}
		>
			{props.children}
		</GlobalContext.Provider>
	);
};
