import axios from 'axios';
import { useEffect, useState } from 'react';
import './App.css';
import ArticleContainer from './components/articleContainer/ArticleContainer';
import Form from './components/form/Form';

function App() {
	const [articles, setArticles] = useState([]);
	const fetchArticles = async () => {
		try {
			const response = await axios.get('http://localhost:8000/api/articles');
			setArticles(response.data.data)
		} catch (error) {
			console.log(error);
		}
	};

	useEffect(() => {
		fetchArticles();
	}, []);

	return (
		<div className="App">
			<Form articles={articles} setArticles={setArticles} fetchArticles={fetchArticles} />
			<ArticleContainer articles={articles} />
		</div>
	);
}

export default App;
