import React from 'react';
import Article from '../article/Article';
import './articleContainer.css';

function ArticleContainer(props) {
	return (
		<div className="article-container">
			{props.articles.map(article => (
				<Article article={article} />
			))}
		</div>
	);
}

export default ArticleContainer;
